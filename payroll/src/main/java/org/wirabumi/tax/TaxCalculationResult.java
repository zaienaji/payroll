package org.wirabumi.tax;

import java.math.BigDecimal;

public class TaxCalculationResult {
    
    private BigDecimal incomeTax;
    private BigDecimal incomeTaxAllowance;
    
    public TaxCalculationResult(BigDecimal incomeTax, BigDecimal incomeTaxAllowance) {
	super();
	this.incomeTax = incomeTax;
	this.incomeTaxAllowance = incomeTaxAllowance;
    }

    public BigDecimal getIncomeTax() {
        return incomeTax;
    }

    public BigDecimal getIncomeTaxAllowance() {
        return incomeTaxAllowance;
    };
    
}
