package org.wirabumi.payroll;

import java.math.BigDecimal;

import javax.management.OperationsException;

import org.wirabumi.common.ContractException;
import org.wirabumi.tax.TaxCalculationResult;
import org.wirabumi.tax.TaxCalculator;
import org.wirabumi.tax.TaxDimension;

public class YearEndSalary extends Salary {

    public YearEndSalary(TaxDimension taxDimension, TaxCalculator taxCalculator)
	    throws ContractException {
	super(taxDimension, taxCalculator);
    }

    @Override
    BigDecimal grossIncome() {
	return getRecurringPay()
		.add(getNonRecurringPay())
		.add(getAccumulatedPrevRecurringPay())
		.add(getAccumulatedPrevNonRecurringPay());
    }

    @Override
    TaxCalculationResult calculateTax() throws OperationsException, ContractException {
	TaxCalculationResult a = taxCalculator.calculateTax(taxDimension, grossIncome());
	BigDecimal incomeTax = a.getIncomeTax().subtract(getTaxPaidInAdvance());
	
	return new TaxCalculationResult(incomeTax, BigDecimal.ZERO); //TODO calculate tax allowance
    }
    
}
