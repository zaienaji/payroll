package org.wirabumi.payroll;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.management.OperationsException;

import org.wirabumi.common.ContractException;
import org.wirabumi.tax.TaxCalculationResult;
import org.wirabumi.tax.TaxCalculator;
import org.wirabumi.tax.TaxDimension;

public class NonRecurringOnlySalary extends Salary {

    public NonRecurringOnlySalary(TaxDimension taxDimension, TaxCalculator taxCalculator)
	    throws ContractException {
	super(taxDimension, taxCalculator);
    }

    @Override
    BigDecimal grossIncome() {
	return getRecurringPay().multiply(MONTHS_IN_A_YEAR).add(getNonRecurringPay());
    }

    @Override
    TaxCalculationResult calculateTax() throws OperationsException, ContractException {
	TaxCalculationResult yearlyTax = taxCalculator.calculateTax(taxDimension, grossIncome());
	BigDecimal monthlyIncomeTax = yearlyTax.getIncomeTax().divide(MONTHS_IN_A_YEAR, -3, RoundingMode.UP);
	BigDecimal taxForNonRecurringOnly = monthlyIncomeTax.subtract(getTaxPaidInAdvance());

	//TODO calculate tax allowance for non recurring only
	return new TaxCalculationResult(taxForNonRecurringOnly, BigDecimal.ZERO);
    }

}
